# disable collection_artifact by default
%bcond_with collection_artifact

# disable collection by default since version 3.6.0 (1.3.4)
%bcond_with collection

%if 0%{?fedora} || 0%{?rhel} > 8
%bcond_without html
%else
# pandoc is not supported in rhel 7 and older,
# which is needed for converting .md to .html.
%bcond_with html
%endif

%global collection_name sap_install
%if 0%{?rhel}
%define package_name rhel-system-roles-sap
%global collection_namespace redhat
%else
%define package_name linux-system-roles-sap
%global collection_namespace community
%endif

# collection_version has to be increased by every rebuild
# otherwise, it cannot be uploaded to Automation Hub and Galaxy
# due to version conflict
%define collection_version 1.3.5

%global mainid e2a233f5a0cb68363798bf014b16552cca681bd0
%global commit_id_sap_install %{collection_version}
%global rolename1 sap_general_preconfigure
%global rolename2 sap_ha_install_hana_hsr
%global rolename3 sap_hana_install
%global rolename4 sap_hana_preconfigure
%global rolename5 sap_ha_pacemaker_cluster
%global rolename6 sap_hypervisor_node_preconfigure
%global rolename7 sap_netweaver_preconfigure
%global rolename8 sap_swpm
%global rolename9 sap_vm_preconfigure
%global src_owner linux-system-roles
%global github_repo_sap_install redhat.sap_install
%global rolename_rhel %{rolename1} %{rolename2} %{rolename3} %{rolename4} %{rolename5} %{rolename6} %{rolename7} %{rolename8} %{rolename9} 
%global rolenames %{rolename_rhel}

Name: %{package_name}
Summary: System Roles to configure RHEL for running SAP NetWeaver- or SAP HANA-based products
Version: 3.6.0
Release: 2%{?dist}
License: GPL-3.0-or-later
Url: https://github.com/redhat-sap/community.sap_install
Source0: https://github.com/linux-system-roles/auto-maintenance/archive/%{mainid}/auto-maintenance-%{mainid}.tar.gz
Source1: https://github.com/redhat-sap/redhat.sap_install/archive/%{commit_id_sap_install}/%{github_repo_sap_install}-%{commit_id_sap_install}.tar.gz

# add collection files
Patch1: redhat.sap_install-1.3.5-collections-files-324bfb8bab50d2baa1c4aacdb59e4e5c9e2ba74f.patch

# add tests files
Patch2: redhat.sap_install-1.3.4-add_tests.patch

# add tools files
Patch3: redhat.sap_install-1.3.4-add_tools.patch

BuildArch: noarch

Requires: rhel-system-roles

%if %{with html}
# Requirements for md2html.sh to build the documentation
%if 0%{?fedora} || 0%{?rhel} >= 9
BuildRequires: rubygem-kramdown-parser-gfm
%else
BuildRequires: pandoc
BuildRequires: asciidoc
BuildRequires: highlight
%endif
%endif

# Requirements for galaxy_transform.py
BuildRequires: python3
%if 0%{?fedora} || 0%{?rhel} >= 8
BuildRequires: python3dist(ruamel.yaml)
Requires: python3-jmespath
Requires: python3-netaddr
%else
BuildRequires: python3-ruamel-yaml
Requires: python-jmespath
Requires: python-netaddr
%endif

# NOTE: ansible-core is in rhel-8.6 and newer, but not installable
# in buildroot as it depended on modular Python.
# It has been installable at buildtime in 8.8 and newer.
%if %{with collection}
%if 0%{?rhel} >= 8
BuildRequires: ansible-core >= 2.11.0
Requires: (ansible-core >= 2.11.0 or ansible >= 2.9.0)
%endif
%endif

%if %{undefined __ansible_provides}
Provides: ansible-collection(%{collection_namespace}.%{collection_name}) = %{version}
%endif
# be compatible with the usual Fedora Provides:
Provides: ansible-collection-%{collection_namespace}-%{collection_name} = %{version}-%{release}

# ansible-galaxy is available by ansible-core on RHEL 8.6 and newer at buildtime.
%define ansible_collection_build() ansible-galaxy collection build
%define ansible_collection_install() ansible-galaxy collection install -n -p %{buildroot}%{_datadir}/ansible/collections %{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz

%if 0%{?fedora} || 0%{?rhel} >= 8
%{!?ansible_collection_files:%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}/}
%else
%if %{?ansible_collection_files:0}%{!?ansible_collection_files:1}
%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}/
%endif
%endif

%description
Collection of Ansible roles which configures a RHEL system according
to applicable SAP notes so that any SAP software can be installed.

%if %{with collection_artifact}
%package collection-artifact
Summary: Collection artifact to import to Automation Hub / Ansible Galaxy

%description collection-artifact
Collection artifact for %{name}. This package contains %{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz
%endif

%prep
%setup -q -a1 -n auto-maintenance-%{mainid}

pushd %{github_repo_sap_install}-%{commit_id_sap_install}/
%patch -P1 -p1
%patch -P2 -p1
%patch -P3 -p1
# workaround for md2html error
sed -i -e "s|for POWER ||g" roles/sap_hana_preconfigure/README.md
popd

# remove zero file and symlinks
find . -type f -size 0 -delete
find . -type l -delete

# fix python and bash shebangs
find -type f \( -iname "*.py" \) -exec sed -i '1s=^#! */usr/bin/\(python\|env python\)[23]\?=#!/usr/bin/python3=' {} +
find -type f \( -iname "*.sh" \) -exec sed -i '1s=^#! */bin/bash=#!/usr/bin/bash=' {} +

# remove json files, these are already present in the collection subdirectory
rm -f FILES.json MANIFEST.json

%build
%if %{with html}
readmes=""
for role in %{rolenames}; do
    readmes="${readmes} %{github_repo_sap_install}-%{commit_id_sap_install}/roles/$role/README.md"
done
sh md2html.sh $readmes %{github_repo_sap_install}-%{commit_id_sap_install}/README.md
%endif

%if %{with collection}
# create dest-path
mkdir .collections

for role in %{rolenames} ; do
LANG=en_US.utf-8 LC_ALL=en_US.utf-8 python3 lsr_role2collection.py --role "$role" \
    --src-path %{github_repo_sap_install}-%{commit_id_sap_install}/roles/"$role" \
    --src-owner %{src_owner} \
    --dest-path .collections \
    --namespace %{collection_namespace} \
    --collection %{collection_name} \
    --new-role "$new_role"
done

cp -p galaxy.yml .collections/ansible_collections/%{collection_namespace}/%{collection_name}
# update README.md 
cp %{github_repo_sap_install}-%{commit_id_sap_install}/README.md .collections/ansible_collections/%{collection_namespace}/%{collection_name}

# Build the collection
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
%ansible_collection_build
popd

%endif

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ansible/roles \
         $RPM_BUILD_ROOT%{_pkgdocdir}

# create file selection for documents
echo "%dir %{_pkgdocdir}" > files_section.txt
cp %{github_repo_sap_install}-%{commit_id_sap_install}/README.md $RPM_BUILD_ROOT%{_pkgdocdir}/

%if %{with html}
cp %{github_repo_sap_install}-%{commit_id_sap_install}/README.html $RPM_BUILD_ROOT%{_pkgdocdir}/
%endif
echo "%{_pkgdocdir}/README.*" >> files_section.txt

for role in %{rolenames} ; do
    mkdir -p $RPM_BUILD_ROOT%{_pkgdocdir}/$role
    cp -pR %{github_repo_sap_install}-%{commit_id_sap_install}/roles/$role $RPM_BUILD_ROOT%{_datadir}/ansible/roles/
    install -m 644 %{github_repo_sap_install}-%{commit_id_sap_install}/LICENSE \
        %{github_repo_sap_install}-%{commit_id_sap_install}/roles/$role/README.md \
        $RPM_BUILD_ROOT%{_pkgdocdir}/$role/
%if %{with html}
install -m 644 %{github_repo_sap_install}-%{commit_id_sap_install}/roles/$role/README.html \
    $RPM_BUILD_ROOT%{_pkgdocdir}/$role/
%endif
    echo "%dir %{_pkgdocdir}/$role" >> files_section.txt
done

# install collection files
%if %{with collection}
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
%ansible_collection_install
popd
%else
cp -pR %{github_repo_sap_install}-%{commit_id_sap_install}/collections $RPM_BUILD_ROOT%{_datadir}/ansible/
%endif

# install collection_artifact
%if %{with collection_artifact}
# Copy collection artifact to /usr/share/ansible/collections/ for collection-artifact
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
mv %{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz $RPM_BUILD_ROOT%{_datadir}/ansible/collections/
popd
%endif

%files -f files_section.txt
%dir %{_datadir}/ansible
%dir %{_datadir}/ansible/roles
%doc %{_pkgdocdir}/*/README.md
%license %{_pkgdocdir}/*/LICENSE
%{_datadir}/ansible/roles/*

%if %{with html}
%{_pkgdocdir}/*/README.html
%endif
%if %{with collection}
%{ansible_collection_files}
%else
%{_datadir}/ansible/collections
%endif
%if %{with collection_artifact}
%files collection-artifact
%{_datadir}/ansible/collections/%{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz
%endif

%changelog
* Mon Feb 05 2024 Than Ngo <than@redhat.com> - 3.6.0-2
- sap_hypervisor_node_preconfigure: fix for role name and path for included tasks
- update collection files
Related: RHEL-16897

* Thu Jan 25 2024 Than Ngo <than@redhat.com> - 3.6.0-1
- rebase sap roles to version 1.3.4
- add collection files
- update README files and add missing galaxy.yml, LICENSE
- fix syntax error for defining dependent collections in galaxy.yml
- add tools and tests
Resolves: RHEL-16897

* Mon May 08 2023 Than Ngo <than@redhat.com> - 3.5.0-1
- updated README.md files for sap and sap_swpm roles
- migrated to SPDX license
- converted patch macro use to the most compatible syntax
- rebased sap roles to version 1.2.3
Resolves: #2185037


* Mon Jan 30 2023 Than Ngo <than@redhat.com> - 3.4.0-3
- Related: #2155876, rebase sap roles to version 1.2.1

* Thu Jan 19 2023 Than Ngo <than@redhat.com> - 3.4.0-2
- Related: #2155876, compat-sap-c++-10 is not available for RHEL 8.2 and earlier on hardware platform s390x

* Wed Jan 11 2023 Than Ngo <than@redhat.com> - 3.4.0-1
- Resolves: #2155876, rebase sap roles to version 1.2.0

* Tue Nov 22 2022 Than Ngo <than@redhat.com> - 3.3.0-4
- Resolves: #2142957, drop deprecated paramater for Ansible 2.14

* Fri Jul 15 2022 Than Ngo <than@redhat.com> - 3.3.0-3
- update galaxy.yml and README.md and url
Related: #2103633

* Tue Jul 12 2022 Than Ngo <than@redhat.com> - 3.3.0-2
- make the role Ansible 2.13 compliant
- get rid of all the new ansible-lint 6.3.0 errors for preconfigure and sap_hana_install roles
Resolves: #2103633

* Mon Jul 04 2022 Than Ngo <than@redhat.com> - 3.3.0-1
- Resolves: #2103633, include roles for setting up a two-node SAP HANA pacemaker cluster

* Thu Jun 09 2022 Than Ngo <than@redhat.com> - 3.2.0-3
- sap_hana_preconfigure: Add vars for RHEL 9.1 managed nodes
- sap_general_preconfigure: fix variable inclusion for RHEL minor releases
Related: #2063050

* Wed Jun 08 2022 Than Ngo <than@redhat.com> - 3.2.0-2
- sap_general_preconfigure: do not attempt to enable HANA repos for s390x
- sap_hana_preconfigure: using wrong variable name
- sap_hana_preconfigure: Fix issue sap-linuxlab#154
Related: #2063050

* Mon Mar 21 2022 Than Ngo <than@redhat.com> - 3.2.0-1
- Resolves: #2063050, rebase to new upstream org sap-linuxlab

* Fri Dec 03 2021 Than Ngo <than@redhat.com> - 3.1.1-10
- Related: #2016858, increase release to avoid update conflict

* Fri Dec 03 2021 Than Ngo <than@redhat.com> - 3.1.1-8
- Related: #2016858, rebase

* Tue Nov 16 2021 Than Ngo <than@redhat.com> - 3.1.1-7
- Resolves: #2016858, ansible-lint cleanup

* Wed Sep 08 2021 Than Ngo <than@redhat.com> - 3.1.1-6
- set min kernel version for RHEL 8.4 x86_64
- use grubby for setting tsx=on
- add tsx=on to the kernel command line unconditionally

* Wed Sep 01 2021 Than Ngo <than@redhat.com> - 3.1.1-5
- Related: #1928398, fix yum group install errors on RHEL 7

* Fri Aug 20 2021 Than Ngo <than@redhat.com> - 3.1.1-4
- Related: #1928398, fix #149, #177, #193, #194

* Mon Jul 12 2021 Than Ngo <than@redhat.com> - 3.1.1-3
- add missing __sap_preconfigure_envgroups for RHEL 7.x
- add __sap_hana_preconfigure_req_repos variables for RHEL 8.5 and later
  Related: #1928398

* Thu Jul 01 2021 Than Ngo <than@redhat.com> - 3.1.1-2
- Related: #1928398, add missing changelogs

* Tue Jun 29 2021 Than Ngo <than@redhat.com> - 3.1.1-1
- Resolves: #1928398, rebase to 3.1.1 for Ansible Collections

* Fri Mar 05 2021 Than Ngo <than@redhat.com> - 3.0.1-4
- fixed issue #130 in sap-preconfigure
- fixed typos in README.md in sap_hana_preconfigure
- added detailed explanations for sap_hana_preconfigure_kernel_parameters_ppc64le in README.md
- fixed issue #170 in sap_hana_preconfigure
  Related: #1891642

* Wed Feb 24 2021 Than Ngo <than@redhat.com> - 3.0.1-3
- Related: #1891642, fixed issue #125 in sap-preconfigure and #163 and #164 in sap-hana-preconfigure

* Thu Feb 18 2021 Than Ngo <than@redhat.com> - 3.0.1-2
- Related: #1891642, update sap-hana-preconfigure 3.0.1 and fixed Urls

* Thu Feb 18 2021 Than Ngo <than@redhat.com> - 3.0.1-1
- Related: #1891642, rebase to 3.0.1

* Mon Feb 15 2021 Than Ngo <than@redhat.com> - 3.0.0-2
- Related: #1891642, more fixes #145, #148, #149, #150 #147

* Thu Jan 28 2021 Than Ngo <than@redhat.com> - 3.0.0-1
- Resolves: #1891638, bug fixes
- Resolves: #1891642, new features

* Mon Sep 21 2020 Than Ngo <than@redhat.com> - 2.0.0-7
- Related: #1867027, fix #119

* Thu Aug 20 2020 Than Ngo <than@redhat.com> - 2.0.0-6
- Related: #1867027, fix #92

* Fri Aug 07 2020 Than Ngo <than@redhat.com> - 2.0.0-5
- Resolves: #1867027, more fixes for #89, #109, #110, #111, #112, #113, #114, #116

* Tue Jul 21 2020 Than Ngo <than@redhat.com> - 2.0.0-4
- Related: #1844190, more fixes for #84, #85, #86, #88, #98, #104, #105, #107

* Fri Jul 03 2020 Than Ngo <than@redhat.com> - 2.0.0-3
- Related: #1844190, drop 1944799.yml

* Mon Jun 29 2020 Than Ngo <than@redhat.com> - 2.0.0-2
- Related: #1844190, more fixes for #78, #79, #81, #82, #94-#97, #99, #101-#102

* Mon Jun 08 2020 Than Ngo <than@redhat.com> - 2.0.0-1
- Resolves: #1844190, rebase to 2.0.0

* Thu May 07 2020 Than Ngo <than@redhat.com> - 1.1.1-2
- Resolves: #1829495, fix summary

* Mon Oct 21 2019 Than Ngo <than@redhat.com> - 1.1.1-1
- rebase to 1.1.1
- always use ansible_hostname + ansible_domain instead of ansible_fqdn
- use {{ ansible_hostname }}.{{ ansible_domain }} instead of {{ ansible_fqdn }}
- added LC_ALL=C before commands for which the outputs are to be evaluated

* Thu Sep 12 2019 Than Ngo <than@redhat.com> - 1.1-1
- rebase to 1.1

* Fri Jun 28 2019 Than Ngo <than@redhat.com> - 1.0-1
- Initial release.
